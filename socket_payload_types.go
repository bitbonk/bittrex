package bittrex

import (
	"encoding/json"
	"time"
)

// creating type aliases to be used internally here.
type guid = string
type long = int64

// unmarshal dates.
type date time.Time

func (d *date) UnmarshalJSON(raw []byte) error {
	var timestamp int64
	if timeParseErr := json.Unmarshal(raw, &timestamp); timeParseErr != nil {
		return timeParseErr
	}
	// the timestamp given is in milliseconds.  I wonder if bittrex is assuming node users?
	*d = date(time.Unix(timestamp/1000, 0))
	return nil
}

func (d *date) Get() time.Time {
	return time.Time(*d)
}

// BalanceDelta balance for a given currency
type BalanceDelta struct {
	UUID          guid    `json:"U"`
	AccountID     int     `json:"W"`
	Currency      string  `json:"c"`
	Balance       decimal `json:"b"`
	Available     decimal `json:"a"`
	Pending       decimal `json:"z"`
	CryptoAddress string  `json:"p"`
	Requested     bool    `json:"r"`
	Updated       date    `json:"u"`
	AutoSell      bool    `json:"h"`
}

// Balance response body for balance delta events (uB)
type Balance struct {
	Nonce        int `json:"N"`
	BalanceDelta `json:"d"`
}

type operationType int

// Operation types for listings inside the Exchange payload
const (
	Add = operationType(iota)
	Remove
	Update
	Cancel
)

// ExchangeOrder values describing an order in the order book.
type ExchangeOrder struct {
	Type     operationType `json:"TY"`
	Rate     decimal       `json:"R"`
	Quantity decimal       `json:"Q"`
}

// ExchangeFill values describing fill operations for an order book.
type ExchangeFill struct {
	FillID    int     `json:"FI"`
	OrderType string  `json:"OT"`
	Rate      decimal `json:"R"`
	Quantity  decimal `json:"Q"`
	TimeStamp date    `json:"T"`
}

// ExchangeDelta payload for the "SubscribeToExchangeDeltas" response
type ExchangeDelta struct {
	MarketName string          `json:"M"`
	Nonce      int             `json:"N"`
	Buys       []ExchangeOrder `json:"Z"`
	Sells      []ExchangeOrder `json:"S"`
	Fills      []ExchangeFill  `json:"f"`
}

// ExchangeStateOrder Buy or Sell struct within ExchangeState
type ExchangeStateOrder struct {
	Quantity decimal `json:"Q"`
	Rate     decimal `json:"R"`
}

// ExchangeStateFill Describes a filled order (?) within the ExchangeState
type ExchangeStateFill struct {
	ID        int     `json:"I"`
	TimeStamp date    `json:"T"`
	Quantity  decimal `json:"Q"`
	Price     decimal `json:"P"`
	Total     decimal `json:"t"`
	FillType  string  `json:"F"`
	OrderType string  `json:"OT"`
}

// ExchangeState response payload for use with "QueryExchangeState"
type ExchangeState struct {
	MarketName string               `json:"M"`
	Nonce      int                  `json:"N"`
	Buys       []ExchangeStateOrder `json:"Z"`
	Sells      []ExchangeStateOrder `json:"S"`
	Fills      []ExchangeStateFill  `json:"f"`
}

/*
type ExchangeState struct {
	MarketName string
	Nounce     int
	Buys       []OrderUpdate
	Sells      []OrderUpdate
	Fills      []Fill
	Initial    bool
}

*/

// Order fields describing an individual Order within bittrex
type Order struct {
	UUID              guid    `json:"U"`
	ID                long    `json:"I"`
	OrderUUID         guid    `json:"OU"`
	Exchange          string  `json:"E"`
	OrderType         string  `json:"OT"`
	Quantity          decimal `json:"Q"`
	QuantityRemaining decimal `json:"q"`
	Limit             decimal `json:"X"`
	CommissionPaid    decimal `json:"n"`
	Price             decimal `json:"P"`
	PricePerUnit      decimal `json:"PU"`
	Opened            date    `json:"Y"`
	Closed            date    `json:"C"`
	IsOpen            bool    `json:"i"`
	CancelInitiated   bool    `json:"CI"`
	ImmediateOrCancel bool    `json:"K"`
	IsConditional     bool    `json:"k"`
	Condition         string  `json:"J"`
	ConditionTarget   decimal `json:"j"`
	Updated           date    `json:"u"`
}

// OrderResponse Payload response for Order Delta (uO)
type OrderResponse struct {
	AccountUUID guid  `json:"w"`
	Nonce       int   `json:"N"`
	Type        int   `json:"TY"`
	Order       Order `json:"o"`
}

// Summary Element of the "Deltas" array within the SummaryDeltaResponse struct or "Summaries" array within SummaryQueryResponses
type Summary struct {
	MarketName     string    `json:"M"`
	High           decimal   `json:"H"`
	Low            decimal   `json:"L"`
	Volume         decimal   `json:"V"`
	Last           decimal   `json:"l"`
	BaseVolume     decimal   `json:"m"`
	TimeStamp      Timestamp `json:"T"`
	Bid            decimal   `json:"B"`
	Ask            decimal   `json:"A"`
	OpenBuyOrders  int       `json:"G"`
	OpenSellOrders int       `json:"g"`
	PrevDay        decimal   `json:"PD"`
	Created        date      `json:"x"`
}

// SummaryDeltaResponse response payload for "SubscribeToSummaryDeltas"
type SummaryDeltaResponse struct {
	Nonce  int       `json:"N"`
	Deltas []Summary `json:"D"`
}

// SummaryQueryResponse response payload for "QuerySummaryState"
type SummaryQueryResponse struct {
	Nonce     int       `json:"N"`
	Summaries []Summary `json:"s"`
}

// SummaryLiteDelta Element of the "Deltas" array within the SummaryLite struct
type SummaryLiteDelta struct {
	MarketName string  `json:"M"`
	Last       decimal `json:"l"`
	BaseVolume decimal `json:"m"`
}

// SummaryLite response payload for "SubscribeToSummaryLiteDeltas"
type SummaryLite struct {
	Deltas []SummaryLiteDelta `json:"D"`
}
