package bittrex

import (
	"fmt"
	"net/http"
	"net/url"
	"sync"
	"time"

	"gitlab.com/techviking/signalr/v2"
)

// Client interface type for the bittrex sdk.  Allows for mocking in consuming app tests.
type Client interface {
	// Account Rest Endpoints
	AccountGetBalances() ([]AccountBalance, error)
	AccountGetBalance(string) (AccountBalance, error)
	AccountGetDepositAddress(string) (WalletAddress, error)
	AccountWithdraw(string, decimal, string, string) (TransactionID, error)
	AccountGetOrder(string) (AccountOrderDescription, error)
	AccountGetOrderHistory(string) ([]AccountOrderHistoryDescription, error)
	AccountGetWithdrawalHistory(string) ([]TransactionHistoryDescription, error)
	AccountGetDepositHistory(string) ([]TransactionHistoryDescription, error)

	// Transaction Rest Endpoints
	MarketBuyLimit(string, decimal, decimal) (TransactionID, error)
	MarketSellLimit(string, decimal, decimal) (TransactionID, error)
	MarketCancel(string) (bool, error)
	MarketGetOpenOrders(string) ([]OrderDescription, error)

	// Public Rest Endpoints (mostly instrument info)
	PublicGetMarkets() ([]MarketDescription, error)
	PublicGetCurrencies() ([]Currency, error)
	PublicGetMarketSummaries() ([]MarketSummary, error)
	PublicGetMarketSummary(string) (MarketSummary, error)
	PublicGetOrderBook(string, string) (OrderBook, error)
	PubMarketGetTicks(market string, interval string) ([]Candle, error)
	PubMarketGetSummaries() ([]MarketSummaryV2, error)

	// Socket Setup Methods
	ConnectWebSocket() error
	SubscribeToWebsocketErrors() chan error
	SubscribeToWebsocketHeartbeat() <-chan signalr.Heartbeat
	SubscribeToWebsocketState() <-chan ConnectionState

	// Socket Query Methods
	QueryExchangeState(string) (*ExchangeState, error)
	QuerySummaryState() (*SummaryQueryResponse, error)

	// Socket Subscription Methods
	SubscribeToMarketSummary(string) error
	GetMarketSummaryChan() chan Summary
	SubscribeToMarketSummaryLite(string) error
	GetSummaryLiteDeltaChan() chan SummaryLiteDelta
	SubscribeToExchange(string) error
	GetExchangeDeltasChan() chan ExchangeDelta
	GetBalanceChangesChan() chan BalanceDelta
	GetOrderDeltasChan() chan OrderResponse
}

// bittrex object representing connection to bittrex api.
type bittrex struct {
	apiKey       string
	apiSecret    string
	socketClient signalr.Connection

	ordersDeltaChan  chan OrderResponse
	balanceDeltaChan chan BalanceDelta

	summaryDeltaMutex         sync.RWMutex
	summaryDeltaSubscriptions map[string]struct{}
	summaryDeltaChan          chan Summary
	isSubbedToSummaryDeltas   bool

	exchangeDeltaMutex         sync.RWMutex
	exchangeDeltaSubscriptions map[string]struct{}
	exchangeDeltaChan          chan ExchangeDelta
	isSubbedToExchangeDeltas   bool

	summaryLiteDeltaMutex         sync.RWMutex
	summaryLiteDeltaSubscriptions map[string]struct{}
	summaryLiteDeltaChan          chan SummaryLiteDelta
	isSubbedToSummaryLiteDelta    bool

	errChanMutex sync.RWMutex
	errChan      chan error

	httpClient *http.Client

	stateChanMutex sync.RWMutex
	stateChan      chan ConnectionState
}

// New construct a new client object representing an interface to the various bittrex APIs.
func New(key string, secret string) (Client, error) {
	hc := &http.Client{}
	hc.Timeout = 30 * time.Second

	newClient := &bittrex{
		apiKey:                        key,
		apiSecret:                     secret,
		summaryDeltaSubscriptions:     make(map[string]struct{}),
		summaryDeltaChan:              make(chan Summary, 50),
		summaryLiteDeltaSubscriptions: make(map[string]struct{}),
		summaryLiteDeltaChan:          make(chan SummaryLiteDelta, 50),
		exchangeDeltaSubscriptions:    make(map[string]struct{}),
		exchangeDeltaChan:             make(chan ExchangeDelta, 50),
		errChan:                       make(chan error, 5),
		httpClient:                    hc,
		stateChan:                     make(chan ConnectionState, 5),
	}

	return newClient, nil
}

// ConnectWebSocket provide functionality to connect to the signalr endpoint.
func (b *bittrex) ConnectWebSocket() error {

	socketURL, _ := url.Parse(websocketBaseURI)

	b.socketClient = signalr.New(
		signalr.Config{
			Client:        &http.Client{},
			ConnectionURL: socketURL,
			NegotiatePath: "signalr/negotiate",
			ConnectPath:   "signalr/connect",
			ReconnectPath: "signalr/reconnect",
		},
	)

	go b.listenToStateChanges()
	go b.listenToSocketData()
	go b.listenToSocketErrors()

	return b.socketClient.Connect([]string{websocketHub})
}

// authNewSignalClient authenticate client to retrieve balance and order notifications
func (b *bittrex) authNewSignalClient() error {
	var authContext string
	if err := b.socketClient.CallHub(signalr.CallHubPayload{
		Hub:    websocketHub,
		Method: "GetAuthContext",
		Arguments: []interface{}{
			b.apiKey,
		},
	}, &authContext); err != nil {
		err = AuthError(err.Error())
		b.sendErrorMessage(err)
		return err
	} else if authContext == "" {
		err = AuthError("Unable to authenticate against bittrex api")
		b.sendErrorMessage(err)
		return err
	}

	signedChallenge := b.sign(authContext)

	var challengeOK bool

	if err := b.socketClient.CallHub(signalr.CallHubPayload{
		Hub:    websocketHub,
		Method: "Authenticate",
		Arguments: []interface{}{
			b.apiKey,
			signedChallenge,
		},
	}, &challengeOK); err != nil {
		return AuthError(err.Error())
	} else if !challengeOK {
		err = AuthError("Failed to pass Authentication challenge")
		b.sendErrorMessage(err)
		return err
	}

	return nil
}

func (b *bittrex) listenToStateChanges() {
	for newState := range b.socketClient.SubscribeToState() {
		b.stateChan <- newState
		switch newState {
		case signalr.Broken:
			b.sendErrorMessage(
				NewConnectError("State Changed to bad state", fmt.Errorf("broken")),
			)
			return
		case signalr.Connected:
			if err := b.authNewSignalClient(); err != nil {
				b.sendErrorMessage(err)
			}
		}
	}
}

func (b *bittrex) listenToSocketData() {
	var (
		parseErr             error
		order                = &OrderResponse{}
		balance              = &Balance{}
		exchangeDelta        = &ExchangeDelta{}
		summaryDeltaResponse = &SummaryDeltaResponse{}
		summaryLite          = &SummaryLite{}
	)
	for message := range b.socketClient.ListenToHubResponses() {
		for _, arg := range message.Arguments {
			switch message.Method {
			case eventOrderDelta:
				if parseErr = parseSocketPayload(arg, order); parseErr == nil {
					go b.pipeEventOrderDelta(order)
				} else {
					go b.sendErrorMessage(NewOrderDeltaError(parseErr, arg))
				}
			case eventBalanceDelta:
				if parseErr = parseSocketPayload(arg, balance); parseErr == nil {
					go b.pipeBalanceDelta(balance)
				} else {
					go b.sendErrorMessage(NewBalanceDeltaError(parseErr, arg))
				}
			case eventMarketDelta:
				if parseErr = parseSocketPayload(arg, exchangeDelta); parseErr == nil {
					go b.pipeMarketExchangeDelta(exchangeDelta)
				} else {
					go b.sendErrorMessage(NewMarketExchangeDeltaError(parseErr, arg))
				}
			case eventSummaryDelta:
				if parseErr = parseSocketPayload(arg, summaryDeltaResponse); parseErr == nil {
					go b.pipeEventSummaryDelta(summaryDeltaResponse)
				} else {
					go b.sendErrorMessage(NewSummaryDeltaError(parseErr, arg))
				}
			case eventSummaryDeltaLite:
				if parseErr = parseSocketPayload(arg, summaryLite); parseErr == nil {
					go b.pipeEventSummaryDeltaLite(summaryLite)
				} else {
					go b.sendErrorMessage(NewSummaryLiteDeltaError(parseErr, arg))
				}
			default:
				go b.sendErrorMessage(fmt.Errorf("unknown event type: %s", message.Method))
			}
		}
	}
}

func (b *bittrex) listenToSocketErrors() {
	for err := range b.socketClient.ListenToErrors() {
		go b.sendErrorMessage(err)
	}
}

func (b *bittrex) sendErrorMessage(err error) {
	b.errChanMutex.Lock()
	b.errChan <- err
	b.errChanMutex.Unlock()
}

// SubscribeToWebsocketErrors retrieve reference to error chan for websocket
func (b *bittrex) SubscribeToWebsocketErrors() chan error {
	return b.errChan
}

func (b *bittrex) SubscribeToWebsocketHeartbeat() <-chan signalr.Heartbeat {
	return b.socketClient.ListenToHeartbeat()
}

func (b *bittrex) SubscribeToWebsocketState() <-chan ConnectionState {
	return b.stateChan
}
