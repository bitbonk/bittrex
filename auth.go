package bittrex

import (
	"crypto/hmac"
	"crypto/sha512"
	"encoding/hex"
)

func (b *bittrex) sign(challenge string) string {
	hasher := hmac.New(sha512.New, []byte(b.apiSecret))
	hasher.Write([]byte(challenge))

	return hex.EncodeToString(hasher.Sum(nil))
}
