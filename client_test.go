package bittrex

import (
	"testing"
	"time"
)

func TestNewDefault(t *testing.T) {
	//Assemble
	//Act
	_, err := New("", "")

	//Assert

	if err != nil {
		t.Errorf("Should not have gotten err with default creds:  %s\n", err.Error())
	}
}

// TestBittrex_ConnectWebSocket test function
func TestBittrex_ConnectWebSocket(t *testing.T) {
	//Assemble
	c, _ := New("", "")
	b := c.(*bittrex)
	ticker := time.NewTicker(time.Second * 5)

	//Act

	if err := b.ConnectWebSocket(); err != nil {
		t.Fatalf("error trying to connect %+v", err)
	}


	hb := b.socketClient.ListenToHeartbeat()
	//Assert
	select {
	case e := <-b.errChan:
		switch e.(type) {
		case AuthError:
			//do nothing
			t.Logf("expected error")
		default:
			t.Errorf("Socket error %s\n", e.Error())
		}
	case h := <-hb:
		t.Fatalf("got HEARTBEAT!!!! %s", h)
	case <-ticker.C:
		t.Fatal("ticker error!")
	}

}
