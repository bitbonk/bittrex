package bittrex

import (
	"encoding/json"

	"gitlab.com/techviking/signalr/v2"
)

// QueryExchangeState https://github.com/Bittrex/beta#queryexchangestate
func (b *bittrex) QueryExchangeState(market string) (*ExchangeState, error) {

	var resp json.RawMessage
	if err := b.socketClient.CallHub(signalr.CallHubPayload{
		Hub:    websocketHub,
		Method: "QueryExchangeState",
		Arguments: []interface{}{
			market,
		},
	}, &resp); err != nil {
		return nil, QueryExchangeStateErr{
			originalError: err,
		}
	}

	var state ExchangeState

	parseErr := parseSocketPayload(resp, &state)
	if parseErr != nil {
		return nil, parseErr
	}

	return &state, nil
}

// QuerySummaryState https://github.com/Bittrex/beta#querysummarystate
func (b *bittrex) QuerySummaryState() (*SummaryQueryResponse, error) {
	var resp json.RawMessage
	if err := b.socketClient.CallHub(signalr.CallHubPayload{
		Hub:    websocketHub,
		Method: "QuerySummaryState",
	}, &resp); err != nil {
		return nil, QuerySummaryStateErr{
			originalError: err,
		}
	}

	var summary SummaryQueryResponse
	parseErr := parseSocketPayload(resp, &summary)

	if parseErr != nil {
		return nil, parseErr
	}

	return &summary, nil
}
