package bittrex

import (
	"encoding/json"
	"fmt"
)

// PublicGetMarkets - public/getmarkets
func (b *bittrex) PublicGetMarkets() ([]MarketDescription, error) {

	parsedResponse, parseErr := b.sendRequest("public/getmarkets", nil)

	if parseErr != nil {
		return nil, parseErr
	}

	var response []MarketDescription

	if err := json.Unmarshal(parsedResponse.Result, &response); err != nil {
		return nil, fmt.Errorf("api error - public/getmarkets %s", err.Error())
	}

	// clean out responses with nil values.
	var cleanedResponse []MarketDescription
	defaultVal := MarketDescription{}

	for _, curVal := range response {
		if curVal != defaultVal {
			cleanedResponse = append(cleanedResponse, curVal)
		}
	}

	if len(cleanedResponse) == 0 {
		return nil, fmt.Errorf("validate response - all markets had empty values")
	}

	return cleanedResponse, nil
}

// PublicGetCurrencies - public/getcurrencies
func (b *bittrex) PublicGetCurrencies() ([]Currency, error) {

	parsedResponse, parseErr := b.sendRequest("public/getcurrencies", nil)

	if parseErr != nil {
		return nil, parseErr
	}

	var response []Currency

	if err := json.Unmarshal(parsedResponse.Result, &response); err != nil {
		return nil, fmt.Errorf("api error - public/getcurrencies %s", err.Error())
	}

	// clean out responses with nil values.
	var cleanedResponse []Currency
	defaultVal := Currency{}

	for _, curVal := range response {
		if curVal != defaultVal {
			cleanedResponse = append(cleanedResponse, curVal)
		}
	}

	if len(cleanedResponse) == 0 {
		return nil, fmt.Errorf("validate response - all currencies had empty values")
	}

	return cleanedResponse, nil
}

// PublicGetTicker - public/getticker
func (b *bittrex) PublicGetTicker(market string) (Ticker, error) {
	return Ticker{}, fmt.Errorf("DEPRECATED.  Use the socket api to retrieve exchange deltas for ticker info")
}

// PublicGetMarketSummaries - public/getmarketsummaries
func (b *bittrex) PublicGetMarketSummaries() ([]MarketSummary, error) {

	parsedResponse, parseErr := b.sendRequest("public/getmarketsummaries", nil)

	if parseErr != nil {
		return nil, parseErr
	}

	var response []MarketSummary

	if err := json.Unmarshal(parsedResponse.Result, &response); err != nil {
		return nil, fmt.Errorf("api error - public/getmarketsummaries %s", err.Error())
	}

	// clean out responses with nil values.
	var cleanedResponse []MarketSummary
	defaultVal := MarketSummary{}

	for _, curVal := range response {
		if curVal != defaultVal {
			cleanedResponse = append(cleanedResponse, curVal)
		}
	}

	if len(cleanedResponse) == 0 {
		return nil, fmt.Errorf("validate response - all market summaries had empty values")
	}

	return cleanedResponse, nil
}

// PublicGetMarketSummary - public/getmarketsummary
func (b *bittrex) PublicGetMarketSummary(market string) (MarketSummary, error) {

	parsedResponse, parseErr := b.sendRequest("public/getmarketsummary", map[string]string{"market": market})

	if parseErr != nil {
		return MarketSummary{}, parseErr
	}

	defaultValue := MarketSummary{}

	var response []MarketSummary

	if err := json.Unmarshal(parsedResponse.Result, &response); err != nil {
		return defaultValue, fmt.Errorf("api error - public/getmarketsummary %s", err.Error())
	}

	if response[0] == defaultValue {
		return defaultValue, fmt.Errorf("validate response - market summary had no data")
	}

	return response[0], nil
}

// PublicGetOrderBook - public/getorderbook
func (b *bittrex) PublicGetOrderBook(market string, orderType string) (OrderBook, error) {

	parsedResponse, parseErr := b.sendRequest("/public/getorderbook", map[string]string{"market": market, "type": orderType})
	defaultValue := OrderBook{}

	if parseErr != nil {
		return defaultValue, parseErr
	}

	var response OrderBook

	if err := json.Unmarshal(parsedResponse.Result, &response); err != nil {
		return defaultValue, fmt.Errorf("api error - public/getorderbook %s", err.Error())
	}

	if (response.Buy == nil && response.Sell == nil) || (len(response.Buy) == 0 && len(response.Sell) == 0) {
		return defaultValue, fmt.Errorf("validate response - OrderBook had no data")
	}

	return response, nil
}

// PublicGetMarketHistory - public/getmarkethistory
func (b *bittrex) PublicGetMarketHistory(market string) ([]Trade, error) {

	parsedResponse, parseErr := b.sendRequest("public/getmarkethistory", map[string]string{"market": market})

	if parseErr != nil {
		return nil, parseErr
	}

	var response []Trade

	if err := json.Unmarshal(parsedResponse.Result, &response); err != nil {
		return nil, fmt.Errorf("api error - public/getmarkethistory %s", err.Error())
	}

	// clean out responses with nil values.
	var cleanedResponse []Trade
	defaultVal := Trade{}

	for _, curVal := range response {
		if curVal != defaultVal {
			cleanedResponse = append(cleanedResponse, curVal)
		}
	}

	if len(cleanedResponse) == 0 {
		return nil, fmt.Errorf("validate response - all markets had empty values")
	}

	return cleanedResponse, nil
}
