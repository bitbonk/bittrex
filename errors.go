package bittrex

import (
	"fmt"
)

type AuthError string

func (a AuthError) Error() string {
	return fmt.Sprintf("Bittrex AuthError: %s.  Limiting to public API.", string(a))
}

type socketDataParseError struct {
	error   // must implement order interface.
	prefix  string
	err     error
	payload []byte
}

func (s socketDataParseError) Error() string {
	return fmt.Sprintf("%s: %s", s.prefix, s.err.Error())
}

func (s socketDataParseError) Payload() []byte {
	return s.payload
}

func newSocketDataParseError(prefix string, err error, pl []byte) socketDataParseError {
	return socketDataParseError{
		prefix:  fmt.Sprintf("BittrexError: %s", prefix),
		err:     err,
		payload: pl,
	}
}

type OrderDeltaError socketDataParseError

func NewOrderDeltaError(err error, pl []byte) OrderDeltaError {
  return OrderDeltaError(
    newSocketDataParseError(
      "Unable to parse Order Delta payload",
      err,
      pl,
    ),
  )
}

type BalanceDeltaError socketDataParseError

func NewBalanceDeltaError(err error, pl []byte) BalanceDeltaError {
  return BalanceDeltaError(
    newSocketDataParseError(
      "Unable to parse Balance Delta payload",
      err,
      pl,
    ),
  )
}

type MarketExchangeDeltaError socketDataParseError

func NewMarketExchangeDeltaError(err error, pl []byte) MarketExchangeDeltaError {
  return MarketExchangeDeltaError(
    newSocketDataParseError(
      "Unable to parse Market Exchange Delta payload",
      err,
      pl,
    ),
  )
}

type SummaryDeltaError socketDataParseError

func NewSummaryDeltaError(err error, pl []byte) SummaryDeltaError {
  return SummaryDeltaError(
    newSocketDataParseError(
      "Unable to parse Summary Delta payload",
      err,
      pl,
    ),
  )
}

type SummaryLiteDeltaError socketDataParseError

func NewSummaryLiteDeltaError(err error, pl []byte) SummaryLiteDeltaError {
  return SummaryLiteDeltaError(
    newSocketDataParseError(
      "Unable to parse SummaryLite Delta payload",
      err,
      pl,
    ),
  )
}

// ConnectError error describing issues with underlying signalr connection.
type ConnectError struct {
	error
	prefix string
	msg string
	err error
}

func (c ConnectError) Error() string {
	return fmt.Sprintf("%s: %s - %s", c.prefix, c.msg, c.err)
}

func NewConnectError(msg string, err error) ConnectError {
	return ConnectError {
		prefix: "Bittrex Socket Connect Error",
		msg: msg,
		err: err,
	}
}

type QueryExchangeStateErr struct {
	error
	originalError error
}

func (q QueryExchangeStateErr) Error() string {
	return fmt.Sprintf("QueryExchangeStateErr: %s", q.originalError.Error())
}

type QuerySummaryStateErr struct {
	error
	originalError error
}

func (q QuerySummaryStateErr) Error() string {
	return fmt.Sprintf("QuerySummaryStateErr: %s", q.originalError.Error())
}

