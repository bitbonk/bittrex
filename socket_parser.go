package bittrex

import (
	"bytes"
	"compress/flate"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io"
)

type (
	base64EncodedStr = string

	socketPayloadParser struct {
		err           error
		initialArg    json.RawMessage
		base64Decoded []byte
		buffer        bytes.Buffer
		unzipped      []byte
	}
)

func (p *socketPayloadParser) parse(arg json.RawMessage) ([]byte, error) {
	p.initialArg = arg

	p.base64Decode()
	p.createBuffer()
	p.unzip()

	return p.unzipped, p.err
}

func (p *socketPayloadParser) base64Decode() {

	var unmarshalled base64EncodedStr

	parseErr := json.Unmarshal(p.initialArg, &unmarshalled)

	if parseErr != nil {
		p.err = fmt.Errorf(
			"Unable to convert initial arg to base64EncodedStr \n errmsg: %s \n payload: %s",
			parseErr,
			string(p.initialArg),
		)
		return
	}

	var decodeErr error
	p.base64Decoded, decodeErr = base64.StdEncoding.DecodeString(unmarshalled)

	if decodeErr != nil {
		p.err = fmt.Errorf("unable to decode response contents: %+v", decodeErr)
	}
}

func (p *socketPayloadParser) createBuffer() {
	if p.err != nil {
		return
	}

	_, bufWriteErr := p.buffer.Write(p.base64Decoded)
	if bufWriteErr != nil {
		p.err = fmt.Errorf("unable to write decoded string to buffer: %+v", bufWriteErr)
	}
}

func (p *socketPayloadParser) unzip() {
	if p.err != nil {
		return
	}

	var buf bytes.Buffer

	zr := flate.NewReader(&p.buffer)

	_, readErr := io.Copy(&buf, zr)

	if readErr != nil {
		p.err = fmt.Errorf("unable to unzip response contents. %+v", readErr)
		return
	}

	p.unzipped = make([]byte, buf.Len())

	_, readErr2 := buf.Read(p.unzipped)

	if readErr2 != nil {
		p.unzipped = nil
		p.err = fmt.Errorf("unable to read from buffer %+v", readErr2)
		return
	}

}

// parseSocketPayload engage the socket payload parser.
func parseSocketPayload(arg json.RawMessage, payload interface{}) error {
	var (
		parser  socketPayloadParser
		rawData []byte
		err     error
	)
	parser = socketPayloadParser{}

	if rawData, err = parser.parse(arg); err != nil {
		return err
	}

	if err = json.Unmarshal(rawData, payload); err != nil {
		return fmt.Errorf("unknown event body --- %s --- \n error: %s", string(rawData), err.Error())
	}

	return nil
}
